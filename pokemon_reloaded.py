from tkinter import *
import os
import pygame
from tkinter import messagebox
import random
import pickle
import time
import plateada_ciudad


pygame.init()
pygame.mixer.init()
pygame.mixer.music.load("mus_presentacion_oak.ogg")
pygame.mixer.music.play(10)



# load_img
# Entradas: nombre de la imagen que se desea llamarap
# Salidas: la imagen formato .png que fue  llamada
def load_img(name):
    path = os.path.join("imgs", name)
    img = PhotoImage(file=path)
    return img

# ventana principal

ventana_principal = Tk()
ventana_principal.title("Pokemon Reloaded")
ventana_principal.minsize(850, 550)
ventana_principal.resizable(width=False, height=False)
ventana_principal.config(bg='black')
# canvas sobre el cual va la interfaz grafica
canvas_principal = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
canvas_principal.config(bg="black")
canvas_principal.pack()
canvas_paleta = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
canvas_paleta.config(bg="white")
canvas_lab = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
canvas_lab.config(bg="white")
canvas_bosque = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
canvas_bosque.config(bg="black")
canvas_batalla = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
canvas_batalla.config(bg="black")
canvas_ciudad_plateda = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
canvas_ciudad_plateda.config(bg="black")
caja_pokemon = load_img("caja_pokemon.png")
canvas_pokedex = Canvas(ventana_principal, width=411, height=397, bd=0, highlightthickness=0)
canvas_pokedex.create_image(0, 0, anchor=NW, image=caja_pokemon)

# Rectangulos creados para las colisiones del laboratorio y sus bbox
reclab = canvas_lab.create_rectangle(0, 0, 850, 130)
rec1lab = canvas_lab.create_rectangle(0, 290, 190, 130)
rec2lab = canvas_lab.create_rectangle(0, 450, 250, 340)
rec3lab = canvas_lab.create_rectangle(600, 450, 850, 340)
rec4lab = canvas_lab.create_rectangle(600, 275, 760, 205)
rec5lab = canvas_lab.create_rectangle(820, 0, 850, 550)
rec6lab = canvas_lab.create_rectangle(0, 0, 30, 550)
rec7lab = canvas_lab.create_rectangle(0, 520, 375, 850)
rec8lab = canvas_lab.create_rectangle(475, 520, 850, 850)
rec9lab = canvas_lab.create_rectangle(433, 130, 473, 190)
reclab_bbox = canvas_lab.bbox(reclab)
rec1lab_bbox = canvas_lab.bbox(rec1lab)
rec2lab_bbox = canvas_lab.bbox(rec2lab)
rec3lab_bbox = canvas_lab.bbox(rec3lab)
rec4lab_bbox = canvas_lab.bbox(rec4lab)
rec5lab_bbox = canvas_lab.bbox(rec5lab)
rec6lab_bbox = canvas_lab.bbox(rec6lab)
rec7lab_bbox = canvas_lab.bbox(rec7lab)
rec8lab_bbox = canvas_lab.bbox(rec8lab)
rec9lab_bbox = canvas_lab.bbox(rec9lab)

# lista de imagenes
lista_imagenes = [load_img("intro_oak1.png"), load_img("intro_oak2.png"), load_img("intro_oak3.png"),
                  load_img("intro_oak4.png"),
                  load_img("intro_oak5.png"), load_img("intro_oak6.png"), load_img("intro_oak7.png"),
                  load_img("intro_oak8.png"), load_img("intro_oak9.png"), load_img("intro_oak10.png"),
                  load_img("intro_oak11.png"), load_img("intro_oak12.png"), load_img("intro_oak13.png"),
                  load_img("intro_oak14.png"), load_img("intro_oak15.png"), load_img("intro_oak16.png"),
                  load_img("intro_oak17.png"),
                  load_img("intro_oak18.png"), load_img("intro_oak19.png"), load_img("intro_oak20.png"),
                  load_img("intro_oak21.png"),
                  load_img("intro_oak22.png"), load_img("intro_oak23.png"), load_img("interior_lab.png")]

# Carga la imagen de inicio
pic1 = load_img("pantalla_inicio.png")
canvas_principal.create_image(0, 0, anchor=NW, image=pic1, tag="inicio")
global pika_paleta, ash_paleta

# press_enter
# Entradas: si se presiona la tecla "Enter"
# Salidas: devuelve a la funcion de las presentaciones del inicio
def press_enter(event):
    if "<Return>":
        return presentacion()


ventana_principal.bind("<Return>", press_enter)

# Asignacion de la variable para mostrar el escenario de fondo
num_imagen = 0


# presentacion
# Entradas: ninguna
# Salidas: muestra la imagen correspondiente y captura el nombre del personaje, al final retorna la funcion laboratorio
def presentacion():
    global num_imagen, entrada, guarda_nombre, nombre, pokedex_pokemon
    pokedex_pokemon = {"blastoise": 0, "charizard": 0, "venasaur": 0,
                       "pidgeot": 0, "pikachu": 1, "raichu": 0,
                       "raticate": 0}
    if num_imagen == 3:
        contador = 0
        ventana_principal.unbind("<Return>")
        while contador < 7:
            canvas_principal.itemconfig("inicio", image=lista_imagenes[num_imagen])
            time.sleep(0.02)
            num_imagen += 1
            canvas_principal.update()
            contador += 1
        ventana_principal.bind("<Return>", press_enter)
    if num_imagen == 15:
        canvas_principal.itemconfig("inicio", image=lista_imagenes[num_imagen])
        entrada_nombre = StringVar()
        nombre = Entry(canvas_principal, textvariable=entrada_nombre)
        nombre.place(x=470, y=150)
    elif num_imagen == 16:
        global guarda_nombre
        guarda_nombre = nombre.get()
        nombre.destroy()
        canvas_principal.itemconfig("inicio", image=lista_imagenes[num_imagen])
    elif num_imagen == 17:
        ventana_principal.unbind("<Return>")
        contador = 0
        while contador < 7:
            canvas_principal.itemconfig("inicio", image=lista_imagenes[num_imagen])
            time.sleep(0.4)
            num_imagen += 1
            canvas_principal.update()
            contador += 1
        ventana_principal.bind("<Return>", press_enter)
        canvas_principal.destroy()
        pygame.mixer.music.stop()
        return laboratorio(445, 200, 0, 0)
    else:
        canvas_principal.itemconfig("inicio", image=lista_imagenes[num_imagen])
    num_imagen += 1


# Creacion del canvas para el laboratorio, , el laboratorio y el personaje
lab = load_img("interior_lab.png")
canvas_lab.create_image(0, 0, anchor=NW, image=lab, tag="labora")
estante_lab = load_img("estante_lab.png")
estante_lab1 = canvas_lab.create_image(652, 375, anchor=CENTER, image=estante_lab)
estante_lab2 = canvas_lab.create_image(86, 375, anchor=CENTER, image=estante_lab)
estante_lab3 = canvas_lab.create_image(764, 375, anchor=CENTER, image=estante_lab)
estante_lab4 = canvas_lab.create_image(198, 375, anchor=CENTER, image=estante_lab)
ash1 = load_img("ash_e_1.png")
mensaje = Label(canvas_lab, text="Recoge tu pokemon").place(x=625, y=210)
poke = load_img("pokeball_sprite.png")
pokeball = canvas_lab.create_image(670, 240, anchor=NW, image=poke)
pokeball_bbox = canvas_lab.bbox(pokeball)
pika1 = load_img("pikachu_f_1.png")


# laboratorio
# Entradas: ninguna
# Salidas: muestra el canvas del laboratorio junto con los objetos creados en el
def laboratorio(x_ash, y_ash, x_pika, y_pika):
    global ash_lab, canvas, ash_lab
    canvas = "contenedor_lab"
    ash_lab = canvas_lab.create_image(x_ash, y_ash, anchor=CENTER, image=ash1)
    pygame.mixer.music.load("mus_lab.ogg")
    pygame.mixer.music.play(10)
    canvas_lab.pack()
    movimiento_lab_ash()
    ventana_principal.unbind("<Return>")


# lista de objetos del laboratorio
lista_objetos_lab = [reclab_bbox, rec1lab_bbox, rec2lab_bbox, rec3lab_bbox, rec4lab_bbox, rec5lab_bbox,
                     rec6lab_bbox, rec7lab_bbox, rec8lab_bbox,
                     rec9lab_bbox]

# listas de los sprites de movimiento del personaje y pikachu
mover_arriba1, mover_derecha1, mover_izq1 = [load_img("ash_e_1.png"), load_img("ash_e_2.png"),
                                             load_img("ash_e_3.png"), ], \
                                            [load_img("ash_d_2.png"), load_img("ash_d_2.png"),
                                             load_img("ash_d_3.png"), ], \
                                            [load_img("ash_i_1.png"), load_img("ash_i_2.png"),
                                             load_img("ash_i_2.png")]

mover_abajo1 = [load_img("ash_f_1.png"), load_img("ash_f_2.png"), load_img("ash_f_3.png")]

mover_arriba2, mover_abajo2, mover_derecha2, mover_izq2 = [load_img("pikachu_e_1.png"), load_img("pikachu_e_2.png"),
                                                           load_img("pikachu_e_3.png"), ], \
                                                          [load_img("pikachu_f_1.png"), load_img("pikachu_f_2.png"),
                                                           load_img("pikachu_f_3.png")], \
                                                          [load_img("pikachu_d_2.png"), load_img("pikachu_d_2.png"),
                                                           load_img("pikachu_d_3.png"), ], \
                                                          [load_img("pikachu_i_1.png"), load_img("pikachu_i_2.png"),
                                                           load_img("pikachu_i_2.png")]


# elimina_sprite
# Entradas: ninguna
# Salidas: elimina el sprite de la pokebola cuando el personaje se ubica frente a ella y crea el pikachu a la par
def elimina_sprite():
    canvas_lab.delete(pokeball)
    global pika_lab
    pika_lab = canvas_lab.create_image(715, 295, anchor=CENTER, image=pika1)


# crear_pueblo_paleta
# Entradas: ninguna
# Salidas: creacion de la interfaz del pueblo
def crear_pueblo_paleta_desde_lab(x_ash, y_ash, x_pika, y_pika):
    pygame.mixer.music.stop()
    canvas_lab.destroy()
    pygame.mixer.music.load("mus_pueblo_paleta.ogg")
    pygame.mixer.music.play(10)
    global pika_paleta, ash_paleta, imagencasa, imagencasa2, lab_sprite1, canvas
    canvas = "contenedor_paleta"
    pika_paleta = canvas_paleta.create_image(x_pika, y_pika, anchor=CENTER, image=pika1)
    ash_paleta = canvas_paleta.create_image(x_ash, y_ash, anchor=CENTER, image=ash1)
    canvas_paleta.pack()
    movimiento_paleta()


# crear_pueblo_paleta
# Entradas: ninguna
# Salidas: creacion de la interfaz del pueblo
def crear_pueblo_paleta_desde_bosque():
    global pika_paleta, ash_paleta
    pygame.mixer.music.stop()
    canvas_bosque.forget()
    pygame.mixer.music.load("mus_pueblo_paleta.ogg")
    pygame.mixer.music.play(10)
    canvas_paleta.pack()
    movimiento_paleta()



intro_bosque = load_img("bosque_verde_intro.png")
canvas_intro_bosque = Canvas(ventana_principal, width=850, height=550, bd=0, highlightthickness=0)
canvas_intro_bosque.config(bg="white")
canvas_intro_bosque.create_image(0, 0, anchor=NW, image=intro_bosque)
# crear_bosque_verde
# Entradas: ninguna
# Salidas: creacion de la interfaz del bosque
def crear_bosque_verde(x_ash, y_ash, x_pika, y_pika):
    global pika_bosque, ash_bosque, canvas
    canvas_paleta.forget()
    contador = 0
    canvas_intro_bosque.pack()
    while contador < 1:
        ventana_principal.unbind("<space>")
        ventana_principal.unbind("<Up>")
        ventana_principal.unbind("<Left>")
        ventana_principal.unbind("<Right>")
        ventana_principal.unbind("<Down>")
        pygame.mixer.music.load("mus_bosque_verde.ogg")
        pygame.mixer.music.play(10)
        canvas_intro_bosque.update()
        time.sleep(1)
        contador += 1
        canvas_intro_bosque.update()
    canvas_intro_bosque.forget()
    canvas_bosque.pack()
    canvas = "contenedor_bosque"
    pika_bosque = canvas_bosque.create_image(x_pika, y_pika, anchor=CENTER, image=pika1)
    ash_bosque = canvas_bosque.create_image(x_ash, y_ash, anchor=CENTER, image=ash1)
    aparecer_poke()
    movimiento_bosque()


def crear_ciudad_plateada():
    canvas_bosque.unbind("<space>")
    canvas_bosque.unbind("<Up>")
    canvas_bosque.unbind("<Left>")
    canvas_bosque.unbind("<Right>")
    canvas_bosque.unbind("<Down>")
    canvas_bosque.forget()
    return  plateada_ciudad.ciudad_plateada(ventana_principal, pokedex_pokemon, guarda_nombre)



# calcular_coords_x
# Entradas: coordenada x1 y x2 del objeto
# Salidas: una lista con todas las posibles coordenadas de x en el objeto
def calcular_coords_x(coordenada_x1, coordenada_x2, lista_coordx):
    if coordenada_x1 > coordenada_x2:
        return lista_coordx
    else:
        lista_coordx.append(coordenada_x1)
        return calcular_coords_x(coordenada_x1 + 1, coordenada_x2, lista_coordx)


# calcular_coords_y
# Entradas: coordenada y1 y y2 del objeto
# Salidas: una lista con todas las posibles coordenadas de y en el objeto
def calcular_coords_y(coordenada_y1, coordenada_y2, lista_coordy):
    if coordenada_y1 > coordenada_y2:
        return lista_coordy
    else:
        lista_coordy.append(coordenada_y1)
        return calcular_coords_y(coordenada_y1 + 1, coordenada_y2, lista_coordy)


# Asignacion de la variable para las transiciones de ash y pikachu{
numero_imagen_pika = 0

numero_imagen_ash = 0


# movimiento_lab
# Entradas: ninguna
# Salidas: funciones con los movimientos de los personajes en el laboratorio
def movimiento_lab_ash():
    # movimiento_ash_lab
    # Entradas: sprites de los movimientos del personaje dependiendo hacia donde se mueva y el fondo del escenario
    # Salidas: el sprite del movimiento del personaje
    ash_lab = globals()["ash_lab"]

    def movimiento_ash_lab(movimientos, fondo):
        global numero_imagen_ash
        numero_imagen_ash += 1
        if numero_imagen_ash == len(movimientos):
            numero_imagen_ash = 0
        fondo.itemconfig(ash_lab, image=movimientos[numero_imagen_ash])

    # colisiones_arriba_lab_ash
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de ash
    def colisiones_arriba_lab_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if pokeball_bbox[1] <= canvas_lab.coords(ash_lab)[1] - 30 < pokeball_bbox[3]:
            if canvas_lab.coords(ash_lab)[0] in calcular_coords_x(pokeball_bbox[0], pokeball_bbox[2], []):
                elimina_sprite()
        if lista_objetos[0][1] <= canvas_lab.coords(ash_lab)[1] - 18 < lista_objetos[0][3]:
            if canvas_lab.coords(ash_lab)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_arriba_lab_ash(lista_objetos[1:])
        else:
            return colisiones_arriba_lab_ash(lista_objetos[1:])

    # colisiones_abajo_lab_ash
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de ash
    def colisiones_abajo_lab_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][1] <= canvas_lab.coords(ash_lab)[1] + 18 < lista_objetos[0][3]:
            if canvas_lab.coords(ash_lab)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_abajo_lab_ash(lista_objetos[1:])
        else:
            return colisiones_abajo_lab_ash(lista_objetos[1:])

    # colisiones_derecha_lab_ash
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de ash
    def colisiones_derecha_lab_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][0] <= canvas_lab.coords(ash_lab)[0] + 18 < lista_objetos[0][2]:
            if canvas_lab.coords(ash_lab)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_derecha_lab_ash(lista_objetos[1:])
        else:
            return colisiones_derecha_lab_ash(lista_objetos[1:])

    # colisiones_izquierda_lab_ash
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de ash
    def colisiones_izquierda_lab_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][0] <= canvas_lab.coords(ash_lab)[0] - 18 < lista_objetos[0][2]:
            if canvas_lab.coords(ash_lab)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_izquierda_lab_ash(lista_objetos[1:])
        else:
            return colisiones_izquierda_lab_ash(lista_objetos[1:])

    # mover_arriba
    # Entradas: si se presiona la tecla: "Arrow-Up"
    # Salidas: llama la funcion de movimiento y mueve el personaje hacia arriba 7 pixeles
    def mover_arriba(event):
        if canvas_lab.coords(ash_lab) == [682.0, 291.0]:
            elimina_sprite()
        if colisiones_arriba_lab_ash(lista_objetos_lab) == False:
            movimiento_ash_lab(mover_arriba1, canvas_lab)
            canvas_lab.move(ash_lab, 0, 0)
        else:
            movimiento_ash_lab(mover_arriba1, canvas_lab)
            canvas_lab.move(ash_lab, 0, -7)

    # mover_abajo
    # Entradas: si se presiona la tecla: "Arrow-Down"
    # Salidas: llama la funcion de movimiento y mueve el personaje hacia abajo 7 pixeles
    def mover_abajo(event):
        if canvas_lab.coords(ash_lab) == [682.0, 291.0]:
            elimina_sprite()
        if canvas_lab.coords(ash_lab)[1] == 515:
            return crear_pueblo_paleta_desde_lab(565, 440, 535, 447)
        if colisiones_abajo_lab_ash(lista_objetos_lab) == False:
            movimiento_ash_lab(mover_abajo1, canvas_lab)
            canvas_lab.move(ash_lab, 0, 0)
        else:
            movimiento_ash_lab(mover_abajo1, canvas_lab)
            canvas_lab.move(ash_lab, 0, 7)

    # mover_derecha
    # Entradas: si se presiona la tecla: "Arrow-Right"
    # Salidas: llama la funcion de movimiento y mueve el personaje hacia la derecha 7 pixeles
    def mover_derecha(event):
        if colisiones_derecha_lab_ash(lista_objetos_lab) == False:
            movimiento_ash_lab(mover_derecha1, canvas_lab)
            canvas_lab.move(ash_lab, 0, 0)
        else:
            movimiento_ash_lab(mover_derecha1, canvas_lab)
            canvas_lab.move(ash_lab, 7, 0)

    # mover_izquierda
    # Entradas: si se presiona la tecla: "Arrow-Left"
    # Salidas: llama la funcion de movimiento y mueve el personaje hacia la izquierda 7 pixeles
    def mover_izquierda(event):
        if canvas_lab.coords(ash_lab) == [682.0, 291.0]:
            elimina_sprite()
        if colisiones_izquierda_lab_ash(lista_objetos_lab) == False:
            movimiento_ash_lab(mover_izq1, canvas_lab)
            canvas_lab.move(ash_lab, 0, 0)
        else:
            movimiento_ash_lab(mover_izq1, canvas_lab)
            canvas_lab.move(ash_lab, -7, 0)

        #    ventana_principal.unbind("<space>")

    ventana_principal.bind("<space>", menu)
    ventana_principal.bind("<Up>", mover_arriba)
    ventana_principal.bind("<Left>", mover_izquierda)
    ventana_principal.bind("<Right>", mover_derecha)
    ventana_principal.bind("<Down>", mover_abajo)


# Rectangulos creados para las colisiones del pueblo y sus bbox
rec_paleta = canvas_paleta.create_rectangle(166, 65, 340, 220)
rec1_paleta = canvas_paleta.create_rectangle(470, 55, 640, 200)
rec2_paleta = canvas_paleta.create_rectangle(465, 270, 670, 440)
rec3_paleta = canvas_paleta.create_rectangle(155, 290, 370, 430)
rec4_paleta = canvas_paleta.create_rectangle(0, 0, 100, 550)
rec5_paleta = canvas_paleta.create_rectangle(740, 0, 850, 550)
rec6_paleta = canvas_paleta.create_rectangle(100, 535, 740, 550)
rec7_paleta = canvas_paleta.create_rectangle(0, 0, 650, 10)
rec8_paleta = canvas_paleta.create_rectangle(510, 0, 600, 55)
rec9_paleta = canvas_paleta.create_rectangle(206, 0, 293, 65)
rec_bbox_paleta = canvas_paleta.bbox(rec_paleta)
rec1_bbox_paleta = canvas_paleta.bbox(rec1_paleta)
rec2_bbox_paleta = canvas_paleta.bbox(rec2_paleta)
rec3_bbox_paleta = canvas_paleta.bbox(rec3_paleta)
rec4_bbox_paleta = canvas_paleta.bbox(rec4_paleta)
rec5_bbox_paleta = canvas_paleta.bbox(rec5_paleta)
rec6_bbox_paleta = canvas_paleta.bbox(rec6_paleta)
rec7_bbox_paleta = canvas_paleta.bbox(rec7_paleta)
rec8_bbox_paleta = canvas_paleta.bbox(rec8_paleta)
rec9_bbox_paleta = canvas_paleta.bbox(rec9_paleta)

# interfaz grafica del pueblo
pueblo_paleta1 = load_img("paleta.png")
canvas_paleta.create_image(0, 0, anchor=NW, image=pueblo_paleta1)
casa_sprite1 = load_img("casa_sprite.png")
imagencasa = canvas_paleta.create_image(132, 6, anchor=NW, image=casa_sprite1)
imagencasa2 = canvas_paleta.create_image(435, -8, anchor=NW, image=casa_sprite1)
lab_sprite = load_img("lab_sprite.png")
lab_sprite1 = canvas_paleta.create_image(445, 228, anchor=NW, image=lab_sprite)

# lista de los bbox en palea
lista_objetos_paleta = [rec_bbox_paleta, rec1_bbox_paleta, rec2_bbox_paleta, rec3_bbox_paleta, rec4_bbox_paleta,
                        rec5_bbox_paleta, rec6_bbox_paleta, rec7_bbox_paleta, rec8_bbox_paleta, rec9_bbox_paleta]


# movimiento_paleta
# Entradas: ninguna
# Salidas: funciones con los movimientos de los personajes en pueblo paleta
def movimiento_paleta():
    ash_paleta
    # movimiento_ash_paleta
    # Entradas: sprites de los movimientos del personaje dependiendo hacia donde se mueva y el fondo del escenario
    # Salidas: el sprite del movimiento del personaje
    def movimiento_ash_paleta(movimientos, fondo):
        global numero_imagen_ash
        numero_imagen_ash += 1
        if numero_imagen_ash == len(movimientos):
            numero_imagen_ash = 0
        fondo.itemconfig(ash_paleta, image=movimientos[numero_imagen_ash])

    # movimiento_pika_paleta
    # Entradas: sprites de los movimientos de pikachu dependiendo hacia donde se mueva y el fondo del escenario
    # Salidas: el sprite del movimiento de pikachu
    def movimiento_pika_paleta(movimientos, fondo):
        global numero_imagen_pika
        numero_imagen_pika += 1
        if numero_imagen_pika == len(movimientos):
            numero_imagen_pika = 0
        fondo.itemconfig(pika_paleta, image=movimientos[numero_imagen_pika])

    # colisiones_arriba_paleta_pika
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de pikachu
    def colisiones_arriba_paleta_pika(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][1] <= canvas_paleta.coords(pika_paleta)[1] - 18 < lista_objetos[0][3]:
            if canvas_paleta.coords(pika_paleta)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_arriba_paleta_pika(lista_objetos[1:])
        else:
            return colisiones_arriba_paleta_pika(lista_objetos[1:])

    # colisiones_abajo_paleta_pika
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de pikachu
    def colisiones_abajo_paleta_pika(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][1] <= canvas_paleta.coords(pika_paleta)[1] + 18 < lista_objetos[0][3]:
            if canvas_paleta.coords(pika_paleta)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_abajo_paleta_pika(lista_objetos[1:])
        else:
            return colisiones_abajo_paleta_pika(lista_objetos[1:])

    # colisiones_derecha_paleta_pika
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de pikachu
    def colisiones_derecha_paleta_pika(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][0] <= canvas_paleta.coords(pika_paleta)[0] + 18 < lista_objetos[0][2]:
            if canvas_paleta.coords(pika_paleta)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_derecha_paleta_pika(lista_objetos[1:])
        else:
            return colisiones_derecha_paleta_pika(lista_objetos[1:])

    # colisiones_arriba_paleta_pika
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de pikachu
    def colisiones_izquierda_paleta_pika(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][0] <= canvas_paleta.coords(pika_paleta)[0] - 18 < lista_objetos[0][2]:
            if canvas_paleta.coords(pika_paleta)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_izquierda_paleta_pika(lista_objetos[1:])
        else:
            return colisiones_izquierda_paleta_pika(lista_objetos[1:])

    # colisiones_arriba_paleta_ash
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de ash
    def colisiones_arriba_paleta_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][1] <= canvas_paleta.coords(ash_paleta)[1] - 18 < lista_objetos[0][3]:
            if canvas_paleta.coords(ash_paleta)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_arriba_paleta_ash(lista_objetos[1:])
        else:
            return colisiones_arriba_paleta_ash(lista_objetos[1:])

    # colisiones_abajo_paleta_pika
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de ash
    def colisiones_abajo_paleta_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][1] <= canvas_paleta.coords(ash_paleta)[1] + 18 < lista_objetos[0][3]:
            if canvas_paleta.coords(ash_paleta)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_abajo_paleta_ash(lista_objetos[1:])
        else:
            return colisiones_abajo_paleta_ash(lista_objetos[1:])

    # colisiones_derecha_paleta_ash
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de ash
    def colisiones_derecha_paleta_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][0] <= canvas_paleta.coords(ash_paleta)[0] + 18 < lista_objetos[0][2]:
            if canvas_paleta.coords(ash_paleta)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_derecha_paleta_ash(lista_objetos[1:])
        else:
            return colisiones_derecha_paleta_ash(lista_objetos[1:])

    # colisiones_izquierda_paleta_ash
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de ash
    def colisiones_izquierda_paleta_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][0] <= canvas_paleta.coords(ash_paleta)[0] - 18 < lista_objetos[0][2]:
            if canvas_paleta.coords(ash_paleta)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_izquierda_paleta_ash(lista_objetos[1:])
        else:
            return colisiones_izquierda_paleta_ash(lista_objetos[1:])

    # mover_arriba
    # Entradas: evento de la tecla arriba
    # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
    def mover_arriba(event):
        if canvas_paleta.coords(ash_paleta)[1] <= -27:
            return crear_bosque_verde(780, 660, 750, 665)
        if colisiones_arriba_paleta_ash(lista_objetos_paleta) == False or colisiones_arriba_paleta_pika(
                lista_objetos_paleta) == False:
            movimiento_ash_paleta(mover_arriba1, canvas_paleta)
            canvas_paleta.move(ash_paleta, 0, 0)
            movimiento_pika_paleta(mover_arriba2, canvas_paleta)
            canvas_paleta.move(pika_paleta, 0, 0)
        else:
            movimiento_ash_paleta(mover_arriba1, canvas_paleta)
            canvas_paleta.move(ash_paleta, 0, -7)
            movimiento_pika_paleta(mover_arriba2, canvas_paleta)
            canvas_paleta.move(pika_paleta, 0, -7)

    # mover_abajo
    # Entradas: evento de la tecla abajo
    # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
    def mover_abajo(event):
        if colisiones_abajo_paleta_ash(lista_objetos_paleta) == False or colisiones_abajo_paleta_pika(
                lista_objetos_paleta) == False:
            movimiento_ash_paleta(mover_abajo1, canvas_paleta)
            canvas_paleta.move(ash_plateada, 0, 0)
            movimiento_pika_paleta(mover_abajo2, canvas_paleta)
            canvas_paleta.move(pika_plateada, 0, 0)
        else:
            movimiento_ash_paleta(mover_abajo1, canvas_paleta)
            canvas_paleta.move(ash_paleta, 0, 7)
            movimiento_pika_paleta(mover_abajo2, canvas_paleta)
            canvas_paleta.move(pika_paleta, 0, 7)

    # mover_derecha
    # Entradas: evento de la tecla derecha
    # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
    def mover_derecha(event):
        if colisiones_derecha_paleta_ash(lista_objetos_paleta) == False or colisiones_derecha_paleta_pika(
                lista_objetos_paleta) == False:
            movimiento_ash_paleta(mover_derecha1, canvas_paleta)
            canvas_paleta.move(ash_paleta, 0, 0)
            movimiento_pika_paleta(mover_derecha2, canvas_paleta)
            canvas_paleta.move(pika_paleta, 0, 0)
        else:
            movimiento_ash_paleta(mover_derecha1, canvas_paleta)
            canvas_paleta.move(ash_paleta, 7, 0)
            movimiento_pika_paleta(mover_derecha2, canvas_paleta)
            canvas_paleta.move(pika_paleta, 7, 0)

    # mover_izquierda
    # Entradas: evento de la tecla izquierda
    # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
    def mover_izquierda(event):
        if colisiones_izquierda_paleta_ash(lista_objetos_paleta) == False or colisiones_izquierda_paleta_pika(
                lista_objetos_paleta) == False:
            movimiento_ash_paleta(mover_izq1, canvas_paleta)
            canvas_paleta.move(ash_paleta, 0, 0)
            movimiento_pika_paleta(mover_izq2, canvas_paleta)
            canvas_paleta.move(pika_paleta, 0, 0)
        else:
            movimiento_ash_paleta(mover_izq1, canvas_paleta)
            canvas_paleta.move(ash_paleta, -7, 0)
            movimiento_pika_paleta(mover_izq2, canvas_paleta)
            canvas_paleta.move(pika_paleta, -7, 0)

    ventana_principal.bind("<space>", menu)
    ventana_principal.bind("<Up>", mover_arriba)
    ventana_principal.bind("<Left>", mover_izquierda)
    ventana_principal.bind("<Right>", mover_derecha)
    ventana_principal.bind("<Down>", mover_abajo)


# rectangulos para las colisiones en el bosque
rec = canvas_bosque.create_rectangle(0, 0, 850, 60)
rec1 = canvas_bosque.create_rectangle(0, 0, 40, 550)
rec2 = canvas_bosque.create_rectangle(0, 535, 715, 550)
rec3 = canvas_bosque.create_rectangle(820, 0, 850, 550)
rec4 = canvas_bosque.create_rectangle(525, 350, 625, 550)
rec5 = canvas_bosque.create_rectangle(720, 310, 850, 440)
rec6 = canvas_bosque.create_rectangle(510, 113, 750, 250)
rec7 = canvas_bosque.create_rectangle(380, 0, 430, 400)
rec8 = canvas_bosque.create_rectangle(255, 172, 300, 470)
rec9 = canvas_bosque.create_rectangle(125, 0, 170, 480)
rec10 = canvas_bosque.create_rectangle(285, 430, 330, 550)
rec11 = canvas_bosque.create_rectangle(330, 467, 525, 550)
rec12 = canvas_bosque.create_rectangle(0, 0, 170, 125)
rec13 = canvas_bosque.create_rectangle(40, 0, 120, 160)

# cuadrantes en los cuales aparecen los pokemones en el bosque
cuadrante1 = canvas_bosque.create_rectangle(40, 220, 125, 512)
cuadrante2 = canvas_bosque.create_rectangle(170, 200, 255, 473)
cuadrante3 = canvas_bosque.create_rectangle(300, 200, 380, 397)
cuadrante4 = canvas_bosque.create_rectangle(430, 200, 510, 360)
cuadrante5 = canvas_bosque.create_rectangle(528, 260, 590, 350)

# interfaz del bosque
bosque_verde1 = load_img("bosque_verde.png")
bosque_verde = canvas_bosque.create_image(0, 0, anchor=NW, image=bosque_verde1)

fondo_batalla1 = load_img("campo_batalla_100.png")
fondo_batalla2 = load_img("campo_batalla_85.png")
fondo_batalla3 = load_img("campo_batalla_70.png")
fondo_batalla4 = load_img("campo_batalla_55.png")
fondo_batalla5 = load_img("campo_batalla_25.png")
blastoise = load_img("blastoise.png")
blastoise2 = load_img("blastoise_grande.png")
charizard = load_img("charizard.png")
charizard2 = load_img("charizard_grande.png")
venasaur = load_img("venasaur.png")
venasaur2 = load_img("venasaur_grande.png")
raticate = load_img("raticate.png")
raticate2 = load_img("raticate_grande.png")
pikachu_batalla = load_img("pikachu_pelea.png")
pikachu_batalla2 = load_img("pikachu_grande.png")
raichu = load_img("raichu.png")
raichu2 = load_img("raichu_grande.png")
pidgeot = load_img("pidgeot.png")
pidgeot2 = load_img("pidgeot_grande.png")
# Bbox de los rectangulos de las colisiones
rec_bbox = canvas_bosque.bbox(rec)
rec1_bbox = canvas_bosque.bbox(rec1)
rec2_bbox = canvas_bosque.bbox(rec2)
rec3_bbox = canvas_bosque.bbox(rec3)
rec4_bbox = canvas_bosque.bbox(rec4)
rec5_bbox = canvas_bosque.bbox(rec5)
rec6_bbox = canvas_bosque.bbox(rec6)
rec7_bbox = canvas_bosque.bbox(rec7)
rec8_bbox = canvas_bosque.bbox(rec8)
rec9_bbox = canvas_bosque.bbox(rec9)
rec10_bbox = canvas_bosque.bbox(rec10)
rec11_bbox = canvas_bosque.bbox(rec11)
rec12_bbox = canvas_bosque.bbox(rec12)
rec13_bbox = canvas_bosque.bbox(rec13)

# lista de objetos
lista_objetos_bosque = [rec_bbox, rec1_bbox, rec2_bbox, rec3_bbox, rec4_bbox, rec5_bbox, rec6_bbox, rec7_bbox,
                        rec8_bbox, rec9_bbox, rec10_bbox, rec11_bbox, rec12_bbox]

# lista de pokemones
lista_pokemones = [blastoise, charizard, venasaur, pikachu_batalla, raichu, pidgeot, raticate]

# lista de pokemones
lista_pokemones2 = [blastoise2, charizard2, venasaur2, pikachu_batalla2, raichu2, pidgeot2, raticate2]

# lista de cuadrantes en la hierba
lista_cuadrante1 = [[66, 247], [66, 450], [66, 380], [66, 275], [66, 303], [66, 331], [66, 415], [66, 457], [66, 450]]

lista_cuadrante2 = [[197, 240], [197, 420], [197, 373], [197, 296], [197, 345], [197, 401], [197, 429]]

lista_cuadrante3 = [[323, 219], [323, 323], [323, 254], [323, 324], [323, 219]]

lista_cuadrante4 = [[453, 226], [453, 303], [453, 261], [453, 220]]


# randomizar_pokemones
# Entradas: lista de pokemones
# Salidas: cuatro pokemones
def randomizar_pokemones(lista1):
    aleatorio = random.choice(lista1)
    return aleatorio


# randomizar_posiciones
# Entradas: lista de posiciones del primer cuadrante
# Salidas: una posicion
def randomizar_posiciones1(lista1):
    aleatorio = random.choice(lista1)
    return aleatorio


# randomizar_posiciones
# Entradas: lista de posiciones del segundo cuadrante
# Salidas: una posicion
def randomizar_posiciones2(lista1):
    aleatorio = random.choice(lista1)
    return aleatorio


# randomizar_posiciones
# Entradas: lista de posiciones del tercer cuadrante
# Salidas: una posicion
def randomizar_posiciones3(lista1):
    aleatorio = random.choice(lista1)
    return aleatorio


# randomizar_posiciones
# Entradas: lista de posiciones del cuarto cuadrante
# Salidas: una posicion
def randomizar_posiciones4(lista1):
    aleatorio = random.choice(lista1)
    return aleatorio


# aparecer_poke
# Entradas: ninguna
# Salidas: los 4 pokemones en distintas posiciones y iniciar la batalla contra este
def aparecer_poke():
    global random_pos1, random_pos2, random_pos3, random_pos4, poke1_bbox, poke2_bbox, poke3_bbox, poke4_bbox, poke1, poke2, poke3, poke4, etiqueta
    pygame.mixer.music.stop()
    pygame.mixer.music.load("mus_bosque_verde.ogg")
    pygame.mixer.music.play(10)
    canvas_bosque.delete("poke1")
    canvas_bosque.delete("poke2")
    canvas_bosque.delete("poke3")
    canvas_bosque.delete("poke4")
    random_pos1 = randomizar_posiciones1(lista_cuadrante1)
    poke1 = randomizar_pokemones(lista_pokemones)
    poke_1 = canvas_bosque.create_image(random_pos1, anchor=CENTER, image=poke1, tag="poke1")
    poke1_bbox = canvas_bosque.bbox(poke_1)
    random_pos2 = randomizar_posiciones2(lista_cuadrante2)
    poke2 = randomizar_pokemones(lista_pokemones)
    poke_2 = canvas_bosque.create_image(random_pos2, anchor=CENTER, image=poke2, tag="poke2")
    poke2_bbox = canvas_bosque.bbox(poke_2)
    random_pos3 = randomizar_posiciones3(lista_cuadrante3)
    poke3 = randomizar_pokemones(lista_pokemones)
    poke_3 = canvas_bosque.create_image(random_pos3, anchor=CENTER, image=poke3, tag="poke3")
    poke3_bbox = canvas_bosque.bbox(poke_3)
    random_pos4 = randomizar_posiciones4(lista_cuadrante4)
    poke4 = randomizar_pokemones(lista_pokemones)
    poke_4 = canvas_bosque.create_image(random_pos4, anchor=CENTER, image=poke4, tag="poke4")
    poke4_bbox = canvas_bosque.bbox(poke_4)
    canvas_bosque.pack()
    global fondo_batalla01
    fondo_batalla01 = canvas_batalla.create_image(0, 0, anchor=NW, image=fondo_batalla1, tag="fondo_batalla")


lista_batalla = [fondo_batalla1, fondo_batalla2, fondo_batalla3, fondo_batalla4, fondo_batalla5]

# Asignacion de la variable para mostrar el escenario de fondo
num_img = 1


# atacar_boton
# Entradas: click
# Salidas: ataque de pikachu
def atacar_button():
    global num_img
    if num_img == 5:
        canvas_batalla.place(x=999999999999, y=999999999999)
        num_img = 0
        aparecer_poke()
        movimiento_bosque()
    else:
        canvas_batalla.itemconfig("fondo_batalla", image=lista_batalla[num_img])
    num_img += 1

def atrapar_button():
    global num_img
    if num_img >= 4:
        if pokemon_actual == charizard:
            pokedex_pokemon["charizard"] += 1
            canvas_batalla.place(x=999999999999, y=999999999999)
            movimiento_bosque()
            aparecer_poke()
            num_img = 1
        elif pokemon_actual == blastoise:
            pokedex_pokemon["blastoise"] += 1
            canvas_batalla.place(x=999999999999, y=999999999999)
            movimiento_bosque()
            aparecer_poke()
            num_img = 1
        elif pokemon_actual == raichu:
            pokedex_pokemon["raichu"] += 1
            canvas_batalla.place(x=999999999999, y=999999999999)
            movimiento_bosque()
            aparecer_poke()
            num_img = 1
        elif pokemon_actual == venasaur:
            pokedex_pokemon["venasaur"] += 1
            canvas_batalla.place(x=999999999999, y=999999999999)
            movimiento_bosque()
            aparecer_poke()
            num_img = 1
        elif pokemon_actual == raticate:
            pokedex_pokemon["raticate"] += 1
            canvas_batalla.place(x=999999999999, y=999999999999)
            movimiento_bosque()
            aparecer_poke()
        elif pokemon_actual == pidgeot:
            pokedex_pokemon["pidgeot"] += 1
            canvas_batalla.place(x=999999999999, y=999999999999)
            movimiento_bosque()
            aparecer_poke()
            num_img = 1
        elif pokemon_actual == pikachu_batalla:
            pokedex_pokemon["pikachu"] += 1
            canvas_batalla.place(x=999999999999, y=999999999999)
            movimiento_bosque()
            aparecer_poke()
            num_img = 1
        aparecer_poke()
        movimiento_bosque()


# huir_boton
# Entradas: click
# Salidas: huir de la pelea
def huir_button():
    global mover_abajo, mover_arriba, mover_derecha, mover_izquierda
    canvas_batalla.place(x=9999999,y=999999)
    num_img = 0
    aparecer_poke()
    movimiento_bosque()


# campo_batalla
# Entradas: ninguna
# Salidas: campo de batalla

def campo_batalla(pokemon):
    global pokemon_actual
    pokemon_actual = pokemon
    for x in range(len(lista_pokemones)):
        if lista_pokemones[x] == pokemon_actual:
            pokemon = lista_pokemones2[x]
            canvas_batalla.create_image(630, 290, anchor=CENTER, image=pokemon)
    pygame.mixer.music.stop()

    pygame.mixer.music.load("mus_batalla.ogg")
    pygame.mixer.music.play(10)
    ataque = Button(canvas_batalla, width=10, height=1, command=atacar_button, text="Impactrueno", bg="white",
                    fg="black").place(x=550, y=500)
    capturar = Button(canvas_batalla, width=7, height=1, command=atrapar_button, text="capturar", bg="white",
                      fg="black").place(x=650, y=500)
    huir = Button(canvas_batalla, width=7, height=1, command=huir_button, text="Huir", bg="white",
                  fg="black").place(x=725, y=500)
    ventana_principal.unbind("<space>")
    ventana_principal.unbind("<Up>")
    ventana_principal.unbind("<Left>")
    ventana_principal.unbind("<Right>")
    ventana_principal.unbind("<Down>")
    canvas_batalla.place(x=0,y=0)


# movimiento_bosque
# Entradas: ninguna
# Salidas: funciones con los movimientos de los personajes en el bosque verde
def movimiento_bosque():
    # movimiento_ash_bosque
    # Entradas: sprites de los movimientos del personaje dependiendo hacia donde se mueva y el fondo del escenario
    # Salidas: el sprite del movimiento del personaje
    def movimiento_ash_bosque(movimientos, fondo):
        global numero_imagen_ash
        numero_imagen_ash += 1
        if numero_imagen_ash == len(movimientos):
            numero_imagen_ash = 0
        fondo.itemconfig(ash_bosque, image=movimientos[numero_imagen_ash])

    # movimiento_pika_bosque
    # Entradas: sprites de los movimientos del personaje dependiendo hacia donde se mueva y el fondo del escenario
    # Salidas: el sprite del movimiento del personaje
    def movimiento_pika_bosque(movimientos, fondo):
        global numero_imagen_pika
        numero_imagen_pika += 1
        if numero_imagen_pika == len(movimientos):
            numero_imagen_pika = 0
        fondo.itemconfig(pika_bosque, image=movimientos[numero_imagen_pika])

    # colisiones_arriba_bosque_pika
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de pikachu
    def colisiones_arriba_bosque_pika(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if rec13_bbox[1] <= canvas_bosque.coords(pika_bosque)[1] - 18 < rec13_bbox[3]:
            if canvas_bosque.coords(pika_bosque)[0] in calcular_coords_x(rec13_bbox[0], rec13_bbox[2], []):
                return crear_ciudad_plateada()
        if lista_objetos[0][1] <= canvas_bosque.coords(pika_bosque)[1] - 18 < lista_objetos[0][3]:
            if canvas_bosque.coords(pika_bosque)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_arriba_bosque_pika(lista_objetos[1:])
        else:
            return colisiones_arriba_bosque_pika(lista_objetos[1:])

    # colisiones_abajo_bosque_pika
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de pikachu
    def colisiones_abajo_bosque_pika(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][1] <= canvas_bosque.coords(pika_bosque)[1] + 18 < lista_objetos[0][3]:
            if canvas_bosque.coords(pika_bosque)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_abajo_bosque_pika(lista_objetos[1:])
        else:
            return colisiones_abajo_bosque_pika(lista_objetos[1:])

    # colisiones_derecha_bosque_pika
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de pikachu
    def colisiones_derecha_bosque_pika(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][0] <= canvas_bosque.coords(pika_bosque)[0] + 18 < lista_objetos[0][2]:
            if canvas_bosque.coords(pika_bosque)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_derecha_bosque_pika(lista_objetos[1:])
        else:
            return colisiones_derecha_bosque_pika(lista_objetos[1:])

    # colisiones_abajo_bosque_pika
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de pikachu
    def colisiones_izquierda_bosque_pika(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if lista_objetos[0][0] <= canvas_bosque.coords(pika_bosque)[0] - 18 < lista_objetos[0][2]:
            if canvas_bosque.coords(pika_bosque)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_izquierda_bosque_pika(lista_objetos[1:])
        else:
            return colisiones_izquierda_bosque_pika(lista_objetos[1:])

    # colisiones_arriba_bosque_ash
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de ash
    def colisiones_arriba_bosque_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if poke1_bbox[1] <= canvas_bosque.coords(ash_bosque)[1] < poke1_bbox[3] and canvas_bosque.coords(ash_bosque)[
            0] in calcular_coords_x(poke1_bbox[0], poke1_bbox[2], []):
            return campo_batalla(poke1)
        if poke2_bbox[1] <= canvas_bosque.coords(ash_bosque)[1] < poke2_bbox[3] and canvas_bosque.coords(ash_bosque)[
            0] in calcular_coords_x(poke2_bbox[0], poke2_bbox[2], []):
            return campo_batalla(poke2)
        if poke3_bbox[1] <= canvas_bosque.coords(ash_bosque)[1] < poke3_bbox[3] and canvas_bosque.coords(ash_bosque)[
            0] in calcular_coords_x(poke3_bbox[0], poke3_bbox[2], []):
            return campo_batalla(poke3)
        if poke4_bbox[1] <= canvas_bosque.coords(ash_bosque)[1] < poke4_bbox[3] and canvas_bosque.coords(ash_bosque)[
            0] in calcular_coords_x(poke4_bbox[0], poke4_bbox[2], []):
            return campo_batalla(poke4)
        if lista_objetos[0][1] <= canvas_bosque.coords(ash_bosque)[1] - 18 < lista_objetos[0][3]:
            if canvas_bosque.coords(ash_bosque)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_arriba_bosque_ash(lista_objetos[1:])
        else:
            return colisiones_arriba_bosque_ash(lista_objetos[1:])

    # colisiones_abajo_bosque_ash
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de ash
    def colisiones_abajo_bosque_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if poke1_bbox[1] <= canvas_bosque.coords(ash_bosque)[1] < poke1_bbox[3] and canvas_bosque.coords(ash_bosque)[
            0] in calcular_coords_x(poke1_bbox[0], poke1_bbox[2], []):
            canvas_bosque.coords("poke1", 999999999999,999999999)
            return campo_batalla(poke1)
        if poke2_bbox[1] <= canvas_bosque.coords(ash_bosque)[1] < poke2_bbox[3] and canvas_bosque.coords(ash_bosque)[
            0] in calcular_coords_x(poke2_bbox[0], poke2_bbox[2], []):
            return campo_batalla(poke2)
        if poke3_bbox[1] <= canvas_bosque.coords(ash_bosque)[1] < poke3_bbox[3] and canvas_bosque.coords(ash_bosque)[
            0] in calcular_coords_x(poke3_bbox[0], poke3_bbox[2], []):
            return campo_batalla(poke3)
        if poke4_bbox[1] <= canvas_bosque.coords(ash_bosque)[1] < poke4_bbox[3] and canvas_bosque.coords(ash_bosque)[
            0] in calcular_coords_x(poke4_bbox[0], poke4_bbox[2], []):
            return campo_batalla(poke4)
        if lista_objetos[0][1] <= canvas_bosque.coords(ash_bosque)[1] + 18 < lista_objetos[0][3]:
            if canvas_bosque.coords(ash_bosque)[0] in calcular_coords_x(lista_objetos[0][0], lista_objetos[0][2], []):
                return False
            else:
                return colisiones_abajo_bosque_ash(lista_objetos[1:])
        else:
            return colisiones_abajo_bosque_ash(lista_objetos[1:])

    # colisiones_derecha_bosque_ash
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de ash
    def colisiones_derecha_bosque_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if poke1_bbox[0] <= canvas_bosque.coords(ash_bosque)[0] < poke1_bbox[2] and canvas_bosque.coords(ash_bosque)[
            1] in calcular_coords_y(poke1_bbox[1], poke1_bbox[3], []):
            return campo_batalla(poke1)
        if poke2_bbox[0] <= canvas_bosque.coords(ash_bosque)[0] < poke2_bbox[2] and canvas_bosque.coords(ash_bosque)[
            1] in calcular_coords_y(poke2_bbox[1], poke2_bbox[3], []):
            return campo_batalla(poke2)
        if poke3_bbox[0] <= canvas_bosque.coords(ash_bosque)[0] < poke3_bbox[2] and canvas_bosque.coords(ash_bosque)[
            1] in calcular_coords_y(poke3_bbox[1], poke3_bbox[3], []):
            return campo_batalla(poke3)
        if poke4_bbox[0] <= canvas_bosque.coords(ash_bosque)[0] < poke4_bbox[2] and canvas_bosque.coords(ash_bosque)[
            1] in calcular_coords_y(poke4_bbox[1], poke4_bbox[3], []):
            return campo_batalla(poke4)
        if lista_objetos[0][0] <= canvas_bosque.coords(ash_bosque)[0] + 18 < lista_objetos[0][2]:
            if canvas_bosque.coords(ash_bosque)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_derecha_bosque_ash(lista_objetos[1:])
        else:
            return colisiones_derecha_bosque_ash(lista_objetos[1:])

    # colisiones_izquierda_bosque_ash
    # Entradas: la lista con los bbox de los objetos en el mapa
    # Salidas: True o False dependiendo de la posicion de ash
    def colisiones_izquierda_bosque_ash(lista_objetos):
        if len(lista_objetos) == 0:
            return True
        if poke1_bbox[0] <= canvas_bosque.coords(ash_bosque)[0] < poke1_bbox[2] and canvas_bosque.coords(ash_bosque)[
            1] in calcular_coords_y(poke1_bbox[1], poke1_bbox[3], []):
            return campo_batalla(poke1)
        if poke2_bbox[0] <= canvas_bosque.coords(ash_bosque)[0] < poke2_bbox[2] and canvas_bosque.coords(ash_bosque)[
            1] in calcular_coords_y(poke2_bbox[1], poke2_bbox[3], []):
            return campo_batalla(poke2)
        if poke3_bbox[0] <= canvas_bosque.coords(ash_bosque)[0] < poke3_bbox[2] and canvas_bosque.coords(ash_bosque)[
            1] in calcular_coords_y(poke3_bbox[1], poke3_bbox[3], []):
            return campo_batalla(poke3)
        if poke4_bbox[0] <= canvas_bosque.coords(ash_bosque)[0] < poke4_bbox[2] and canvas_bosque.coords(ash_bosque)[
            1] in calcular_coords_y(poke4_bbox[1], poke4_bbox[3], []):
            return campo_batalla(poke4)
        if lista_objetos[0][0] <= canvas_bosque.coords(ash_bosque)[0] - 18 < lista_objetos[0][2]:
            if canvas_bosque.coords(ash_bosque)[1] in calcular_coords_y(lista_objetos[0][1], lista_objetos[0][3], []):
                return False
            else:
                return colisiones_izquierda_bosque_ash(lista_objetos[1:])

    # mover_arriba
    # Entradas: evento de la tecla arriba
    # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
    def mover_arriba(event):
        if colisiones_arriba_bosque_ash(lista_objetos_bosque) == False or colisiones_arriba_bosque_pika(
                lista_objetos_bosque) == False:
            movimiento_ash_bosque(mover_arriba1, canvas_bosque)
            canvas_bosque.move(ash_bosque, 0, 0)
            movimiento_pika_bosque(mover_arriba2, canvas_bosque)
            canvas_bosque.move(pika_bosque, 0, 0)
        else:
            movimiento_ash_bosque(mover_arriba1, canvas_bosque)
            canvas_bosque.move(ash_bosque, 0, -7)
            movimiento_pika_bosque(mover_arriba2, canvas_bosque)
            canvas_bosque.move(pika_bosque, 0, -7)

    # mover_abajo
    # Entradas: evento de la tecla arriba
    # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
    def mover_abajo(event):
        if canvas_bosque.coords(ash_bosque)[1] >= 560:
            return crear_pueblo_paleta_desde_bosque()
        if colisiones_abajo_bosque_ash(lista_objetos_bosque) == False or colisiones_abajo_bosque_pika(
                lista_objetos_bosque) == False:
            movimiento_ash_bosque(mover_abajo1, canvas_bosque)
            canvas_bosque.move(ash_bosque, 0, 0)
            movimiento_pika_bosque(mover_abajo2, canvas_bosque)
            canvas_bosque.move(pika_bosque, 0, 0)
        else:
            movimiento_ash_bosque(mover_abajo1, canvas_bosque)
            canvas_bosque.move(ash_bosque, 0, 7)
            movimiento_pika_bosque(mover_abajo2, canvas_bosque)
            canvas_bosque.move(pika_bosque, 0, 7)

    # mover_derecha
    # Entradas: evento de la tecla arriba
    # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
    def mover_derecha(event):
        if colisiones_derecha_bosque_ash(lista_objetos_bosque) == False or colisiones_derecha_bosque_pika(
                lista_objetos_bosque) == False:
            movimiento_ash_bosque(mover_derecha1, canvas_bosque)
            canvas_bosque.move(ash_bosque, 0, 0)
            movimiento_pika_bosque(mover_derecha2, canvas_bosque)
            canvas_bosque.move(pika_bosque, 0, 0)
        else:
            movimiento_ash_bosque(mover_derecha1, canvas_bosque)
            canvas_bosque.move(ash_bosque, 7, 0)
            movimiento_pika_bosque(mover_derecha2, canvas_bosque)
            canvas_bosque.move(pika_bosque, 7, 0)

    # mover_arriba
    # Entradas: evento de la tecla arriba
    # Salidas: movimiento de ash y pikachu hacia arriba junto con el cambio de imagen de sprite
    def mover_izquierda(event):
        if colisiones_izquierda_bosque_ash(lista_objetos_bosque) == False or colisiones_izquierda_bosque_pika(
                lista_objetos_bosque) == False:
            movimiento_ash_bosque(mover_izq1, canvas_bosque)
            canvas_bosque.move(ash_bosque, 0, 0)
            movimiento_pika_bosque(mover_izq2, canvas_bosque)
            canvas_bosque.move(pika_bosque, 0, 0)
        else:
            movimiento_ash_bosque(mover_izq1, canvas_bosque)
            canvas_bosque.move(ash_bosque, -7, 0)
            movimiento_pika_bosque(mover_izq2, canvas_bosque)
            canvas_bosque.move(pika_bosque, -7, 0)

        #    ventana_principal.unbind("<space>")

    ventana_principal.bind("<space>", menu)
    ventana_principal.bind("<Up>", mover_arriba)
    ventana_principal.bind("<Left>", mover_izquierda)
    ventana_principal.bind("<Right>", mover_derecha)
    ventana_principal.bind("<Down>", mover_abajo)


# canvas_lab.create_image(image=caja_pokemon).place(x=225,y=75)
def lista_pokemon():
    canvas_pokedex.update()
    charizard_cantidad = Label(canvas_pokedex, text=pokedex_pokemon["charizard"], bg="white").place(x=170, y=143)
    raichu_cantidad = Label(canvas_pokedex, text=pokedex_pokemon["raichu"], bg="white").place(x=170, y=355)
    blastoise_cantidad = Label(canvas_pokedex, text=pokedex_pokemon["blastoise"], bg="white").place(x=35, y=143)
    venasaur_cantidad = Label(canvas_pokedex, text=pokedex_pokemon["venasaur"], bg="white").place(x=305, y=143)
    raticate_cantidad = Label(canvas_pokedex, text=pokedex_pokemon["raticate"], bg="white").place(x=305, y=355)
    pidgeot_cantidad = Label(canvas_pokedex, text=pokedex_pokemon["pidgeot"], bg="white").place(x=102.5, y=249)
    pikachu_cantidad = Label(canvas_pokedex, text=pokedex_pokemon["pikachu"], bg="white").place(x=237.5, y=249)
    canvas_pokedex.place(x=225, y=75)


def guardar():
    nombre = guarda_nombre
    canvas = globals()["canvas"]
    if canvas == "contenedor_lab":
        dicc = {'nombre': nombre, 'escenario': 'laboratorio',
                'ubicacion': canvas_lab.coords(ash_lab), 'pokedex':pokedex_pokemon}
        arch = open('save.p', "wb")
        pickle.dump(dicc, arch)
        arch.close()
    elif canvas == "contenedor_paleta":
        dicc = {'nombre': nombre, 'escenario': 'paleta',
                'ubicacion': canvas_paleta.coords(ash_paleta),'picachu': canvas_paleta.coords(pika_paleta) , 'pokedex': pokedex_pokemon}
        arch = open('save.p', "wb")
        pickle.dump(dicc, arch)
        arch.close()
    elif canvas == "contenedor_bosque":
        dicc = {'nombre': nombre, 'escenario': 'bosque',
                'ubicacion': canvas_bosque.coords(ash_bosque),'picachu': canvas_bosque.coords(pika_bosque) , 'pokedex': pokedex_pokemon}
        arch = open('save.p', "wb")
        pickle.dump(dicc, arch)
        arch.close()
    elif canvas == "contenedor_plateda":
        dicc = {'nombre': nombre, 'escenario': 'ciudad_plateada',
                'ubicacion': canvas_plateada.coords(ash_plateada),'picachu': canvas_plateada.coords(pika_plateada) , 'pokedex': pokedex_pokemon}
        arch = open('save.p', "wb")
        pickle.dump(dicc, arch)
        arch.close()


    # continuar_cargar
    # Entradas: Se genera al presionar el boton "Continuar"
    # Salidas: Cargar la partida guardada, justo donde había quedado
    # Restricciones: solo puede cargar los datos que hayan sido guardados


def continuar_cargar():
    global guarda_nombre, pokedex_pokemon
    try:
        arch = open('save.p', "rb")
        dicc = pickle.load(arch)
        arch.close()
        pokedex_pokemon = dicc["pokedex"]
        guarda_nombre = dicc["nombre"]
        canvas_principal.destroy()
        if dicc["escenario"] == "bosque":
            return crear_bosque_verde(dicc["ubicacion"][0], dicc["ubicacion"][1], dicc["picachu"][0], dicc["picachu"][1])
        elif dicc["escenario"] == "laboratorio":
            pygame.mixer.music.stop()
            return laboratorio(dicc["ubicacion"][0], dicc["ubicacion"][1], 0, 0)
        elif dicc["escenario"] == "paleta":
            pygame.mixer.music.stop()
            return crear_pueblo_paleta_desde_lab(dicc["ubicacion"][0], dicc["ubicacion"][1], dicc["picachu"][0], dicc["picachu"][1])
        elif dicc["escenario"] == "ciudad_plateada":
            pygame.mixer.music.stop()
            return crear_ciudad_plateada(dicc["ubicacion"][0], dicc["ubicacion"][1], dicc["picachu"][0], dicc["picachu"][1])
    except:
        return canvas_principal


# menu
# Entradas: tecla space
# Salidas: ventana con el menu junto con los botones de salir y la lista de pokemones
def menu(event):
    menu = Menu(ventana_principal)
    ventana_principal.config(menu=menu)
    filemenu = Menu(menu)
    menu.add_command(label="Pokedex", command=lista_pokemon)
    menu.add_command(label="Guardar", command=guardar)
    menu.add_command(label="Salir", command=exit)
    ventana_principal.bind("<space>", lambda event: (espacio(), menu.destroy()))


def espacio():
    canvas_pokedex.place(x=999999, y=99999)
    ventana_principal.unbind("<space>")
    ventana_principal.bind("<space>", menu)


continuar = Button(canvas_principal, width=10, height=1, command=continuar_cargar, text="Continuar", bg="white",
                fg="black").place(x=550, y=500)

ventana_principal.update()
ventana_principal.mainloop()
